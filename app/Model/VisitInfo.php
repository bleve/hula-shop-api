<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VisitInfo extends Model
{
    protected $table = 'visitInfo';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'belongUserId', 'belongShopId'
    ];

    protected $casts = [
    ];
}
