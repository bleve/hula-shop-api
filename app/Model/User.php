<?php

namespace App\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $table = 'user';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $guard_name = 'web';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'unionId', 'openId','appleId','appOpenId','deviceId', 'addressInfo', 'userInfo','paymentInfo', 'liked', 'status'
    ];

    protected $casts = [
        'userInfo' => 'object',
        'addressInfo' => 'array',
        'paymentInfo' => 'array',
        'liked' => 'array'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['unionId','status','createTime','roles','permission'];
}
