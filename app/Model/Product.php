<?php

namespace App\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Product extends Authenticatable
{


    protected $table = 'product';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'updateTime', 'belongShopId', 'belongCategoriesId', 'title', 'description', 'imgUrls', 'price',
        'storageNum', 'soldNum', 'display', 'status', "unitNum", "unit", "skuId", 'productKind'
    ];

    protected $casts = [
        'imgUrls' => 'array',
        'belongShopId' => 'integer',
        'belongCategoriesId' => 'integer',
        'productKind' => 'object',
        'price' => 'double',
        'storageNum' => 'integer',
        'soldNum' => 'integer',
    ];

    protected $hidden = ['display'];
}
