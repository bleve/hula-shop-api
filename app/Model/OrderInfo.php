<?php

namespace App\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class OrderInfo extends Authenticatable
{

    protected $table = 'orderInfo';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'updateTime','belongShopId', 'belongUserId', 'orderId', 'paymentType', 'paymentWay', 'paymentId', 'productList', 'total',
        'deliveryFee', 'deliveryInfo', 'comment', 'status','paymentImg'
    ];

    protected $casts = [
        'productList' => 'array',
        'belongShopId' => 'integer',
        'belongUserId' => 'integer',
        'total' => 'double',
        'paymentImg'=>'array',
        'deliveryFee' => 'double',
        'deliveryInfo' => 'object',
    ];
}
