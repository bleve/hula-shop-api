<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemInfo extends Model
{
    protected $table = 'itemInfo';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'belongUserId', 'belongShopId','belongProductId','isDone','tapBuy','tapPhone'
    ];

    protected $casts = [
    ];
}
