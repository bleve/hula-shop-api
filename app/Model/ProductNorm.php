<?php

namespace App\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ProductNorm extends Authenticatable
{


    protected $table = 'productNorm';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'belongShopId','belongProductId','name','price','storageNum','soldNum','status'
    ];

    protected $casts = [
    ];

    protected $hidden = ['belongShopId','belongProductId'];
  
}
