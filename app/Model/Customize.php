<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Customize extends Model
{


    protected $table = 'customize';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone','wxId'
    ];

    protected $casts = [
    ];
}
