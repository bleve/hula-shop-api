<?php

namespace App\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;

class Shop extends Authenticatable
{

    use HasApiTokens, Notifiable;
    use HasRoles;

    protected $table = 'shop';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $guard_name = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'belongUserId', 'password', 'username', 'salt', 'name', 'description', 'phone', 'address', 'banner', 'supportInfo', 'email', 'wxId',
        'entry', 'permission', 'status',  'logo', 'background', 'paymentInfo',  'auth'
    ];

    protected $casts = [
        'categories' => 'array',
        'banner' => 'array',
        'supportInfo' => 'array',
        'paymentInfo' => 'array',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['salt', 'password', 'permission', 'roles'];
}
