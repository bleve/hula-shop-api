<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\Shop;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = array('code' => 500, 'errMsg' => 'Request Failed');
        if (!isset($request->auth)) {
            return response()->json($data);
        } else {
            $auth = $request->input('auth');
            $shop = Shop::where('auth', $auth)->first();
            if($shop == null) {return response()->json($data);}
            $request->attributes->add(['belongShopId' => $shop->id]);
        }
        return $next($request);
    }
}
