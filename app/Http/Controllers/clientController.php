<?php

namespace App\Http\Controllers;

use App\Model\OrderInfo;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\Shop;
use App\Model\User;
use PetstoreIO\Order;

class clientController extends Controller
{


    // ———————————————————————————————————————— 商品功能 ——————————————————————————————————————————————————————————————

    // 获取商品list
    public function postFetchProductByPage(Request $request)
    {
        $data = Product::where('display', 'on');
        if (isset($request->title)) {
            $title = $request->input('title');
            $data->where('title', 'Like', '%' . $title . '%');
        }

        if (isset($request->ids)) {
            $ids = $request->input('ids');
            $data->whereIn('id', $ids);
        }

        if (isset($request->belongShopId)) {
            $belongShopId = $request->input('belongShopId');
            $data->where('belongShopId', $belongShopId);
        }

        if (isset($request->productKind) && $request->input('productKind') != NULL) {
            $productKind = $request->input('productKind');
            if (count($productKind) == 1) {
                $data =  $data->whereRaw('JSON_CONTAINS(productKind, \'{"categories":"' . $productKind[0] . '"}\')');
            } else if (count($productKind) == 2) {
                $data =  $data->whereRaw('JSON_CONTAINS(productKind, \'{"categories":"' . $productKind[0] . '", "subCategories":"' . $productKind[1] . '" }\')');
            }
        }

        if (isset($request->orderBy)) {
            $orderBy = $request->input('orderBy');
            $result = $this->orderByData($orderBy);
            $data->orderBy($result[0], $result[1]);
        }

        $data = $data->paginate(20);
        return $this->returnCode(200, $data);
    }

    // 收藏商品
    public function postLikeTheShopProduct(Request $request)
    {
        $result = $this->checkPayload(array("userId", "productId"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }

        $userId = $request->input('userId');
        $productId = $request->input('productId');

        $product = Product::find($productId);
        $user = User::find($userId);

        // if can not find the product or user
        if ($product == null || $user == null) {
            $data = array('code' => -1, 'errMsg' => 'product Id or user Id invalid');
            $output = json_encode($data);
            return $output;
        }

        // add the product to user liked 
        $myLike = $user->myLike;
        // check if user set shop array
        if(!isset($myLike->shop)){
            $myLike->shop = array();
        }

        $checkIfInside = in_array($productId, $myLike->product);
        if ($checkIfInside == false) {
            array_unshift($myLike->product, $product->id);
        } else {
            $tempArr = array();
            foreach ($myLike->product as $item) {
                if ($item != $productId) {
                    $tempArr[] = $item;
                }
            }
            $myLike->product = $tempArr;
        }
        $user->myLike = $myLike;
        $boolUserSave = $user->save();
    }

    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————



    // ———————————————————————————————————————— 订单功能 ——————————————————————————————————————————————————————————————

    // 获取订单list
    public function postFetchOrderByPage(Request $request)
    {
        $data = OrderInfo::query();

        if (isset($request->orderIds)) {
            $orderIds = $request->input('orderIds');
            $data->whereIn('orderId', $orderIds);
        }

        if (isset($request->status)) {
            $status = $request->input('status');
            $data->whereIn('status', $status);
        }

        if (isset($request->belongUserId)) {
            $belongUserId = $request->input('belongUserId');
            $data->where('belongUserId', $belongUserId);
        }

        $data = $data->paginate(20);
        return $this->returnCode(200, $data);
    }


    // 创建新订单
    public function postInsertOrder(Request $request)
    {
        $result = $this->checkPayload(array("belongShopId"), $request->all());
        if ($result->getData()->code == 500) {
            return $result;
        }

        $belongShopId = $request->input('belongShopId');
        if ($this->checkShopRole($belongShopId) == 0) {
            return $this->returnCode(500);
        }

        $obj = (object)$request->all();
        unset($obj->orderId);
        unset($obj->paymentId);

        $orderId = date('YmdHis');
        // 检查重复
        $checkOrder = OrderInfo::where('orderId', $orderId)->count();
        while ($checkOrder > 0) {
            $orderId = date('YmdHis') . strval(rand(0, 100));
            $checkOrder = OrderInfo::where('orderId', $orderId)->count();
        }
        $paymentId = date('YmdHis');
        $checkPayment = OrderInfo::where('paymentId', $paymentId)->count();
        // 检查重复
        while ($checkPayment > 0) {
            $paymentId = date('YmdHis') . strval(rand(0, 100));
            $checkPayment = OrderInfo::where('paymentId', $paymentId)->count();
        }

        $obj->orderId = $orderId;
        $obj->paymentId = $paymentId;
        $obj->status = "pending";

        return $this->insertData('OrderInfo', $obj);
    }


    // 取消订单 
    public function postCancelOrder(Request $request)
    {
        $result = $this->checkPayload(array("belongUserId", "orderId"), $request->all());
        if ($result->getData()->code == 500) {
            return $result;
        }

        $belongUserId = $request->input('belongUserId');
        $orderId = $request->input('orderId');

        $order = OrderInfo::where('belongUserId', $belongUserId)->where('orderId', $orderId)->first();
        if ($order == null) {
            return $this->returnCode(500, null, 'Order not found');
        }
        $order->status = "cancel";
        $order->save();

        return $this->returnCode(200, $order->orderId);
    }

    // 下单前检查商品
    public function postCheckBeforeMakeOrder(Request $request)
    {
        $result = $this->checkPayload(array("productList"), $request->all());
        if ($result->getData()->code == 500) {
            return $result;
        }

        $productList = $request->input('productList');
        foreach ($productList as $item) {
            $product = Product::find($item->id);
            if ($product->status != "on") {
            }
            if ($product->storageNum >= $item->buyNum) {
            }
        }
    }


    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————



}
