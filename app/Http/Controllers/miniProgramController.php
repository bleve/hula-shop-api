<?php

namespace App\Http\Controllers;

use App\Model\Banner;
use App\Model\OrderInfo;
use App\Model\Product;
use App\Model\Shop;
use Illuminate\Http\Request;
use App\Library\wxBizDataCrypt;
use App\Model\ItemInfo;
use App\Model\User;
use App\Model\VisitInfo;
use Illuminate\Support\Str;
use App\Model\Customize;

use AWS;
use Image;
use PetstoreIO\Order;
use Twilio\Rest\Client;

class miniProgramController extends Controller
{


    // ———————————————————————————————————————————————————————————— 用户 ——————————————————————————————————————————————————————————

    // 用户微信登入 --记得改回正式版的hula
    public function postWxLogin(Request $request)
    {

        $result = $this->checkPayload(array("code", "iv", "encryptedData"), $request->all());
        if ($result->getData()->code == 500) {
            return $result;
        }

        $code = $request->input('code');
        $iv = $request->input('iv');
        $encryptedData = $request->input('encryptedData');

        $url = "https://api.weixin.qq.com/sns/jscode2session?appid=wxb75881f7c8929931&secret=9e1be66e0c1afc7905e16a4c76b13dbf&js_code=";
        $url .= $code;
        $url .= "&grant_type=authorization_code";
        $data = file_get_contents($url);
        $data = json_decode($data);
        $sessionKey = $data->session_key;

        $data = "";
        $appid = 'wxb75881f7c8929931';
        $pc = new WXBizDataCrypt($appid, $sessionKey);
        $errCode = $pc->decryptData($encryptedData, $iv, $data);
        $data = json_decode($data);

        $unionId = $data->unionId;
        $openId = $data->openId;
        $gender = $data->gender;

        $obj = new \stdClass();
        $obj->nickName = $data->nickName;
        $obj->avatarUrl = $data->avatarUrl;

        $userDetailObj = new \stdClass();
        $userDetailObj->username = $data->nickName;
        $userDetailObj->avatarUrl = $data->avatarUrl;

        $activeTimeObj = new \stdClass();
        $activeTimeObj->entry = "HULA小店";
        $activeTimeObj->loginTime = time();

        $user = User::where('unionId', $unionId)->first();
        if ($user == null) {
            $userCreate = User::create(array(
                'unionId' => $unionId,
                'openId' => $openId,
                'userInfo' => $obj,
            ));
            $hulaData =  array(
                'unionId' => $unionId, 'openId' => $openId,
                'gender' => $gender, 'activeTime' => $activeTimeObj, 'userDetail' => $userDetailObj
            );
            $callUrl = "https://hula.weboostapp.com/api/v1/registerUser";
            $client = new \GuzzleHttp\Client();
            $response = $client->post(
                $callUrl,
                array(
                    'json' => $hulaData
                )
            );
            $user = User::find($userCreate->id);
            $data->user = $user;
        }
        $data->user = $user;

        if ($errCode == 0) {
            $ouput = array('code' => 0, 'data' => $data);
        } else {
            $ouput = array('code' => -1, 'errMsg' => $errCode);
        }
        $output = json_encode($ouput);
        return $output;
    }

    // 获取用户信息
    public function postFetchUserByIds(Request $request)
    {
        $result = $this->checkPayload(array("ids"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $ids = $request->input('ids');

        $user = User::whereIn('id', $ids)->get();
        $data = array('code' => 0, 'data' => $user);
        $output = json_encode($data);
        return $output;
    }

    // 获取近期访问的五个店铺
    public function postFetchShopByUserId(Request $request)
    {
        $result = $this->checkPayload(array("id"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');

        $visit = VisitInfo::where('belongUserId', $id)->orderBy('createTime', 'DESC')->get();
        $temp = array();
        foreach ($visit as $item) {
            if (count($temp) == 5) {
                break;
            }
            if (in_array($item->belongShopId, $temp)) {
                continue;
            }
            $temp[] = $item->belongShopId;
        }
        if (count($temp) > 0) {
            $tempStr = implode(", ", $temp);
            $shop = Shop::whereIn('id', $temp)->orderByRaw("FIELD(id, " . $tempStr . ")")->get();
            $data = array('code' => 0, 'data' => $shop);
        } else {
            $data = array('code' => 1);
        }

        $output = json_encode($data);
        return $output;
    }

    // 获取用户的收藏商品
    public function postFetchCollectionByUserId(Request $request)
    {
        $result = $this->checkPayload(array("id"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $user = User::find($id);
        $liked = $user->liked;
        $product = Product::where('status', 'on')->whereIn('id', $liked)->get();

        $data = array('code' => 0, 'data' => $product);
        $output = json_encode($data);
        return $output;
    }

    // 获取用户的地址情况
    public function postFetchAddressByUserId(Request $request)
    {
        $result = $this->checkPayload(array("id"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $user = User::find($id);
        $address = $user->address;

        $data = array('code' => 0, 'data' => $address);
        $output = json_encode($data);
        return $output;
    }

    // 获取特定地址情况
    public function postFetchAddressWithIndex(Request $request)
    {
        $result = $this->checkPayload(array("id", "index"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $index = $request->input('index');

        $user = User::find($id);
        $address = $user->address;

        $data = array('code' => 0, 'data' => $address[$index]);
        $output = json_encode($data);
        return $output;
    }


    // 更新特定地址情况或创建新地址
    public function postInsertAddress(Request $request)
    {
        $result = $this->checkPayload(array("id", "addressObj"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $addressObj = $request->input('addressObj');
        $user = User::find($id);
        $address = $user->address;


        if (isset($request->index)) {
            $index = $request->input('index');
            if ($addressObj['ifDefault']) {
                for ($i = 0; $i < count($address); $i++) {
                    $address[$i]['ifDefault'] = false;
                }
            }
            $address[$index] = $addressObj;
        } else {
            if ($address == null || count($address) == 0) {
                $addressObj['ifDefault'] = true;
            } else {
                if ($addressObj['ifDefault']) {
                    for ($i = 0; $i < count($address); $i++) {
                        $address[$i]['ifDefault'] = false;
                    }
                }
            }
            $address[] = $addressObj;
        }



        $user->address = $address;
        $user->save();
        $data = array('code' => 0);
        $output = json_encode($data);
        return $output;
    }

    // 更改默认地址
    public function postSwitchDefaultAddress(Request $request)
    {
        $result = $this->checkPayload(array("id", "index"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $index = $request->input('index');

        $user = User::find($id);
        $address = $user->address;
        for ($i = 0; $i < count($address); $i++) {
            if ($i == $index) {
                $address[$i]['ifDefault'] = true;
            } else {
                $address[$i]['ifDefault'] = false;
            }
        }
        $user->address = $address;
        $user->save();

        $data = array('code' => 0, 'data' => $address);
        $output = json_encode($data);
        return $output;
    }

    // 删除地址
    public function postDeleteAddress(Request $request)
    {
        $result = $this->checkPayload(array("id", "index"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $index = $request->input('index');

        $user = User::find($id);
        $address = $user->address;
        $temp = array();
        for ($i = 0; $i < count($address); $i++) {
            if ($i != $index) {
                $temp[] = $address[$i];
            }
        }
        $user->address = $temp;
        $user->save();

        $data = array('code' => 0, 'data' => $address);
        $output = json_encode($data);
        return $output;
    }



    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————




    // ———————————————————————————————————————————————————————————— 店铺 ——————————————————————————————————————————————————————————

    // 获取店铺通过用户Id
    public function postFetchCertainShopByUserId(Request $request)
    {
        $result = $this->checkPayload(array("id"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $shop = Shop::where('status', 'on')->where('belongUserId', $id)->first();
        $data = array('code' => 0, 'data' => $shop);
        $output = json_encode($data);
        return $output;
    }

    // 获取店铺通过ids
    public function postFetchShopByIds(Request $request)
    {
        $result = $this->checkPayload(array("ids"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $ids = $request->input('ids');

        $shop = Shop::where('status', 'on')->whereIn('id', $ids)->get();
        foreach ($shop as $item) {
            $categories = $item->categories;
            if ($categories != null) {
                usort($categories, function ($a, $b) {
                    return $a['id'] > $b['id'];
                });
                $item->categories = $categories;
            }
        }
        $data = array('code' => 0, 'data' => $shop);
        $output = json_encode($data);
        return $output;
    }

    // 新增开店
    public function postInsertShop(Request $request)
    {
        $result = $this->checkPayload(array("name"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $belongUserId = $request->input('belongUserId');
        $name = $request->input('name');
        $description = $request->input('description');
        $logo = $request->input('logo');
        $background = $request->input('background');
        $phone = $request->input('phone');
        $email = $request->input('email');
        // $cate = $request->input('cate');
        $address = $request->input('address');
        if (isset($request->wxId)) {
            $wxId = $request->input('wxId');
        } else {
            $wxId = "";
        }

        $salt = Str::random(6);
        $shop = Shop::create(array(
            'belongUserId' => $belongUserId,
            'salt' => $salt,
            'name' => $name,
            'description' => $description,
            'logo' => $logo,
            'background' => $background,
            'phone' => $phone,
            'address' => $address,
            'wxId' => $wxId,
            'email' => $email,
            'status' => 'on'
        ));

        $data = array('code' => 0, 'data' => $shop);
        $output = json_encode($data);
        return $output;
    }


    // 检查是否该用户已注册店铺
    public function postCheckIfHaveShopByUserId(Request $request)
    {
        $result = $this->checkPayload(array("id"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');

        $shop = Shop::where('belongUserId', $id)->get();
        $bool = false;
        if (count($shop) > 0) {
            $bool = true;
        }

        $data = array('code' => 0, 'data' => $bool);
        $output = json_encode($data);
        return $output;
    }

    // 获取店铺的分析数据
    public function postFetchShopDataById(Request $request)
    {
        $result = $this->checkPayload(array("id"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');

        $visitAll = VisitInfo::where('belongShopId', $id)->get();
        $visitToday = VisitInfo::where('createTime', '>', date('Y-m-d'))->where('belongShopId', $id)->get();
        $order = OrderInfo::where('updateTime', '>', date('Y-m-d'))->where('belongShopId', $id)->get();

        $obj = new \stdClass();
        $obj->visitAll = count($visitAll);
        $obj->visitToday = count($visitToday);
        $salesToday = 0;
        foreach ($order as $item) {
            if ($item->status != 'pending' && $item->status != 'cancel') {
                $salesToday += $item->total;
            }
        }
        $obj->salesToday = round($salesToday,2);

        $data = array('code' => 0, 'data' => $obj);
        $output = json_encode($data);
        return $output;
    }


    // 修改店铺信息
    public function postUpdateShop(Request $request)
    {
        $result = $this->checkPayload(array("name"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }

        $id = $request->input('id');
        $name = $request->input('name');
        $description = $request->input('description');
        $logo = $request->input('logo');
        $background = $request->input('background');
        $phone = $request->input('phone');
        $email = $request->input('email');
        $address = $request->input('address');

        $shop = Shop::find($id);

        $shop->name = $name;
        $shop->description = $description;
        $shop->logo = $logo;
        $shop->background = $background;
        $shop->phone = $phone;
        $shop->email = $email;
        $shop->address = $address;
        if (isset($request->wxId)) {
            $wxId = $request->input('wxId');
            $shop->wxId = $wxId;
        }
        $shop->save();

        $data = array('code' => 0, 'data' => $shop);
        $output = json_encode($data);
        return $output;
    }


    // 生成店铺卡片
    public function postGenerateShopPost(Request $request)
    {
        $result = $this->checkPayload(array("qr", "id"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $qr = $request->input('qr');
        $id = $request->input('id');

        $shop = Shop::find($id);
        $logo = $shop->logo;
        $name = $shop->name;
        $bg = $shop->background;


        $img = Image::make("https://cdn.weshop.weboostapp.com/kaidian/RmFQkEO1pSqLyqiV86VGmLJcnHoqs6VD.png");

        $bgImg = Image::make($bg);
        $bgImg->resize(628, 496);
        $img->insert($bgImg, 'top-left', 0, 0);

        $qrImg = Image::make($qr);
        $qrImg->resize(130, 130);
        $img->insert($qrImg, 'bottom-left', 40, 40);

        $logoImg = Image::make($logo);
        $logoImg->resize(140, 140);
        $img->insert($logoImg, 'top', 0, 428);

        // 由于店铺名称长短可能引发的距离问题
        $left = 234;
        $nameLen = strlen($name);
        $left += (12 - $nameLen) * 4;



        $img->text($name, $left, 605, function ($font) {
            $font->file('/home/kaidian/public_html/file/PingFang.ttc');
            $font->size(28);
        });

        $str = Str::random(32);
        $img->save("/tmp/$str");

        $securityPath = $img->dirname . '/';
        // $securityPath = substr($securityPath,0,strlen($securityPath)-1);
        $securityPath = $securityPath . $img->basename;

        $str = Str::random(32);
        $s3 = AWS::createClient('s3');
        $s3->putObject(array(
            'Bucket'     => 'weshop',
            'Key'        => 'kaidian/' . $str . '.png',
            'SourceFile' => $securityPath,
            'ContentEncoding' =>  'base64',
            'ContentType' => 'image/png',
            'ACL' => 'public-read'

        ));

        $temp = new \stdClass();
        $temp->url = "https://cdn.weshop.weboostapp.com/kaidian/" . $str . '.png';

        $data = array('code' => 0, 'data' => $temp);
        $output = json_encode($data);
        return $output;
    }


    // 新增或更新自提点
    public function postInsertPickup(Request $request)
    {
        $result = $this->checkPayload(array("id", "pickup"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $pickup = $request->input('pickup');

        $shop = Shop::find($id);
        $pickupInfo = $shop->pickupInfo;

        if (isset($request->index)) {
            $index = $request->input('index');
            $pickupInfo[$index] = $pickup;
        } else {
            $pickupInfo[] = $pickup;
        }

        $shop->pickupInfo = $pickupInfo;
        $shop->save();


        $data = array('code' => 0);
        $output = json_encode($data);
        return $output;
    }


    // 删除pickup
    public function postDeletePickup(Request $request)
    {
        $result = $this->checkPayload(array("id", "index"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $index = $request->input('index');

        $shop = Shop::find($id);
        $pickupInfo = $shop->pickupInfo;
        $temp = array();
        for ($i = 0; $i < count($pickupInfo); $i++) {
            if ($i != $index) {
                $temp[] = $pickupInfo[$i];
            }
        }
        $shop->pickupInfo = $temp;
        $shop->save();

        $data = array('code' => 0, 'data' => $pickupInfo);
        $output = json_encode($data);
        return $output;
    }


    // 新增或更新配送信息
    public function postInsertDelivery(Request $request)
    {
        $result = $this->checkPayload(array("id", "delivery",  "ifInsert"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $delivery = $request->input('delivery');
        $ifInsert = $request->input('ifInsert');

        $shop = Shop::find($id);
        $deliveryInfo = $shop->deliveryInfo;
        $temp = array();
        $splitData = array();


        // 如果是新增
        if ($ifInsert) {
            // 检查是否存在相同的name
            if ($deliveryInfo != null) {
                foreach ($deliveryInfo as $item) {
                    if ($item['name'] == $delivery['name']) {
                        $data = array('code' => -1);
                        $output = json_encode($data);
                        return $output;
                    }
                }
            }
            // 检查是否存在相同的sub和area
            if ($deliveryInfo != null) {
                foreach ($deliveryInfo as $item) {
                    foreach ($delivery['subInfo'] as $inside) {
                        if (
                            $item['name'] != $delivery['name'] &&
                            $item['sub'] == $inside['sub'] &&
                            $item['area'] == $inside['area']
                        ) {
                            $data = array('code' => -2, 'data' => $item['sub'] . "," . $item['area']);
                            $output = json_encode($data);
                            return $output;
                        }
                    }
                }
            }

            // 如果ok则分散sub和area加入deliveryInfo
            if ($deliveryInfo != null) {
                $temp = $deliveryInfo;
            }
            foreach ($delivery['subInfo'] as $item) {
                $obj = new \stdClass();
                $obj->sub = $item['sub'];
                $obj->area = $item['area'];
                $obj->name = $delivery['name'];
                $obj->type = $delivery['type'];
                $obj->threshold = round(doubleval($delivery['threshold']), 2);
                $obj->price = round(doubleval($delivery['price']), 2);
                array_unshift($splitData, $obj);
            }
        } else {
            // 如果是编辑


            // 如果是编辑的情况先删除旧的
            foreach ($deliveryInfo as $item) {
                if ($item['name'] != $delivery['name']) {
                    $temp[] = $item;
                }
            }
            // 检查是否存在相同的sub和area
            if ($deliveryInfo != null) {
                foreach ($deliveryInfo as $item) {
                    foreach ($delivery['subInfo'] as $inside) {
                        if (
                            $item['name'] != $delivery['name'] &&
                            $item['sub'] == $inside['sub'] &&
                            $item['area'] == $inside['area']
                        ) {
                            $data = array('code' => -2, 'data' => $item['sub'] . "," . $item['area']);
                            $output = json_encode($data);
                            return $output;
                        }
                    }
                }
            }


            // 添加新增的
            foreach ($delivery['subInfo'] as $item) {
                $obj = new \stdClass();
                $obj->sub = $item['sub'];
                $obj->area = $item['area'];
                $obj->name = $delivery['name'];
                $obj->type = $delivery['type'];
                $obj->threshold = round(doubleval($delivery['threshold']), 2);
                $obj->price = round(doubleval($delivery['price']), 2);
                array_unshift($splitData, $obj);
            }
        }


        foreach ($splitData as $item) {
            array_unshift($temp, $item);
        }
        $shop->deliveryInfo = $temp;
        $shop->save();



        $data = array('code' => 0, 'data' => $temp);
        $output = json_encode($data);
        return $output;
    }

    // 删除配送
    public function postDeleteDelivery(Request $request)
    {
        $result = $this->checkPayload(array("id", "index"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $index = $request->input('index');

        $shop = Shop::find($id);
        $deliveryInfo = $shop->deliveryInfo;
        $temp = array();
        for ($i = 0; $i < count($deliveryInfo); $i++) {
            if ($i != $index) {
                $temp[] = $deliveryInfo[$i];
            }
        }
        $shop->deliveryInfo = $temp;
        $shop->save();

        $data = array('code' => 0, 'data' => $deliveryInfo);
        $output = json_encode($data);
        return $output;
    }


    // 新增或更新支付信息
    public function postInsertPayment(Request $request)
    {
        $result = $this->checkPayload(array("id", "payment"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $payment = $request->input('payment');
        $type =  $payment['type'];

        $shop = Shop::find($id);
        $paymentInfo = $shop->paymentInfo;

        $temp = array();
        if ($paymentInfo != null) {
            foreach ($paymentInfo as $item) {
                if ($item->type != $type) {
                    $temp[] = $item;
                }
            }
        }
        $temp[] = $payment;
        $shop->paymentInfo = $temp;
        $shop->save();

        $data = array('code' => 0);
        $output = json_encode($data);
        return $output;
    }

    // 删除支付信息
    public function postDeletePayment(Request $request)
    {
        $result = $this->checkPayload(array("id", "type"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $type = $request->input('type');

        $shop = Shop::find($id);
        $paymentInfo = $shop->paymentInfo;

        $temp = array();
        foreach ($paymentInfo as $item) {
            if ($item->type != $type) {
                $temp[] = $item;
            }
        }
        $shop->paymentInfo = $temp;
        $shop->save();

        $data = array('code' => 0);
        $output = json_encode($data);
        return $output;
    }


    // 新增或更新banner
    public function postInsertBanner(Request $request)
    {
        $result = $this->checkPayload(array("id", "banner"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $banner = $request->input('banner');

        $shop = Shop::find($id);
        $bannerInfo = $shop->banner;

        if (isset($request->index)) {
            $index = $request->input('index');
            $bannerInfo[$index] = $banner;
        } else {
            $bannerInfo[] = $banner;
        }
        $shop->banner = $bannerInfo;
        $shop->save();

        $data = array('code' => 0);
        $output = json_encode($data);
        return $output;
    }

    // 删除banner
    public function postDeleteBanner(Request $request)
    {
        $result = $this->checkPayload(array("id", "index"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $index = $request->input('index');

        $shop = Shop::find($id);
        $bannerInfo = $shop->banner;

        $temp = array();
        for ($i = 0; $i < count($bannerInfo); $i++) {
            if ($i != $index) {
                $temp[] = $bannerInfo[$i];
            }
        }
        $shop->banner = $temp;
        $shop->save();

        $data = array('code' => 0);
        $output = json_encode($data);
        return $output;
    }




    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————



    // ———————————————————————————————————————————————————————————— 商品相关 ——————————————————————————————————————————————————————————

    // 获取商品通过ids
    public function postFetchProductByIds(Request $request)
    {
        $result = $this->checkPayload(array("ids"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $ids = $request->input('ids');

        $product = Product::where('status', 'on')->whereIn('id', $ids)->get();
        $data = array('code' => 0, 'data' => $product);
        $output = json_encode($data);
        return $output;
    }

    // 获取商品信息通过各种条件
    public function postFetchProductBySth(Request $request)
    {
        $result = $this->checkPayload(array("perPageNumber", "belongShopId"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $perPageNumber = $request->input('perPageNumber');
        $belongShopId = $request->input('belongShopId');

        $product = Product::where('status', 'on')->where('belongShopId', $belongShopId);

        if (isset($request->shop)) {
            $product = Product::whereIn('status', array('on', 'off'))->where('belongShopId', $belongShopId);
        }

        if (isset($request->belongCategoriesId)) {
            $belongCategoriesId = $request->input('belongCategoriesId');
            $product =  $product->where('belongCategoriesId', $belongCategoriesId);
        }

        if (isset($request->keyword)) {
            $keyword = $request->input('keyword');
            $product =  $product->where('title', 'LIKE', '%' . $keyword . '%');
        }

        if (isset($request->orderBy)) {
            $orderBy = $request->input('orderBy');
            switch ($orderBy) {
                case "综合":
                    $product =  $product->where('belongCategoriesId', $belongCategoriesId);
                    break;
                case "销量":
                    $product =  $product->orderBy('soldNum', 'DESC');
                    break;
                case "价格":
                    $product =  $product->orderBy('price', 'ASC');
                    break;
            }
        }


        $product = $product->orderBy('updateTime', 'DESC')->orderBy('storageNum', 'DESC')->paginate($perPageNumber);
        $data = array('code' => 0, 'data' => $product);
        $output = json_encode($data);
        return $output;
    }


    // 收藏商品情况
    public function postLikeProduct(Request $request)
    {
        $result = $this->checkPayload(array("type", "userId", "productId"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }

        $type = $request->input('type');
        $userId = $request->input('userId');
        $productId = $request->input('productId');

        $user = User::find($userId);
        $bool = false;
        $liked = $user->liked;


        switch ($type) {
            case "check":
                if ($liked == null) {
                    break;
                }
                if (in_array($productId, $liked)) {
                    $bool = true;
                }
                break;
            case "like":
                if ($liked == null || count($liked) == 0) {
                    $liked = array();
                    $liked[] = $productId;
                    $bool = true;
                } else {
                    $index = array_search($productId, $liked);

                    if (strlen(strval($index)) != 0) {
                        array_splice($liked, $index, 1);
                    } else {
                        $bool = true;
                        $liked[] = $productId;
                    }
                }
                $user->liked = $liked;
                $user->save();
                break;
        }

        $data = array('code' => 0, 'data' => $bool);
        $output = json_encode($data);
        return $output;
    }


    // 新增或更新商品
    public function postInsertProduct(Request $request)
    {
        $result = $this->checkPayload(array("belongShopId", "belongCategoriesId", "title", "description", "price", "storageNum", "imgUrls", "unit"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }

        $belongShopId = $request->input('belongShopId');
        $belongCategoriesId = $request->input('belongCategoriesId');
        $title = $request->input('title');
        $description = $request->input('description');
        $price = $request->input('price');
        $imgUrls = $request->input('imgUrls');
        $storageNum = $request->input('storageNum');
        $unit = $request->input('unit');


        if (isset($request->id)) {
            $id = $request->input('id');
            $product = Product::find($id);
            $product->belongCategoriesId = $belongCategoriesId;
            $product->title = $title;
            $product->description = $description;
            $product->price = $price;
            $product->imgUrls = $imgUrls;
            $product->storageNum = $storageNum;
            $product->unit = $unit;
            $product->save();
        } else {
            $product = Product::create(array(
                "belongShopId" => $belongShopId,
                "belongCategoriesId" => $belongCategoriesId,
                "title" => $title,
                "description" => $description,
                "price" => $price,
                "imgUrls" => $imgUrls,
                "storageNum" => $storageNum,
                "unit" => $unit,
            ));
        }


        $data = array('code' => 0, 'data' => $product);
        $output = json_encode($data);
        return $output;
    }


    // 更新商品状态
    public function postUpdateProductStatus(Request $request)
    {
        $result = $this->checkPayload(array("id", "status"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $status = $request->input('status');

        $product = Product::find($id);
        $product->status = $status;
        $product->save();

        $data = array('code' => 0, 'data' => $product);
        $output = json_encode($data);
        return $output;
    }

    // 批量更新商品状态
    public function postUpdateMultipleProductStatus(Request $request)
    {
        $result = $this->checkPayload(array("ids", "status"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $ids = $request->input('ids');
        $status = $request->input('status');

        $product = Product::whereIn('id', $ids)->update(array(
            'status' => $status
        ));

        $data = array('code' => 0, 'data' => $product);
        $output = json_encode($data);
        return $output;
    }



    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————



    // ———————————————————————————————————————————————————————————— 分类相关 ——————————————————————————————————————————————————————————


    // 获取店铺下的分类(包括首页的分类)
    public function postFetchCateByShopIdandType(Request $request)
    {
        $result = $this->checkPayload(array("id", "type"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $type = $request->input('type');

        $shop = Shop::find($id);
        $cate = $shop->categories;
        usort($cate, function ($a, $b) {
            return $a['id'] > $b['id'];
        });
        $temp = array();
        foreach ($cate as $item) {
            if ($item['home']) {
                $temp[] = $item;
            }
        }
        if ($type == 'home') {
            $data = array('code' => 0, 'data' => $temp);
        } else {
            $data = array('code' => 0, 'data' => $cate);
        }
        $output = json_encode($data);
        return $output;
    }

    // 新增或更新分类
    public function postInsertCate(Request $request)
    {
        $result = $this->checkPayload(array("id", "cate"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $cate = $request->input('cate');

        $shop = Shop::find($id);
        $categories = $shop->categories;

        if (isset($request->index)) {
            $index = $request->input('index');
            for ($i = 0; $i < count($categories); $i++) {
                if ($categories[$i]['id'] == $index) {
                    $categories[$i] = $cate;
                }
            }
        } else {
            $categories[] = $cate;
        }

        $shop->categories = $categories;
        $shop->save();

        $data = array('code' => 0);
        $output = json_encode($data);
        return $output;
    }

    // 删除分类
    public function postDeleteCate(Request $request)
    {
        $result = $this->checkPayload(array("id", "index"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $index = $request->input('index');

        $shop = Shop::find($id);
        $categories = $shop->categories;
        $temp = array();
        for ($i = 0; $i < count($categories); $i++) {
            if ($categories[$i]['id'] != $index) {
                $temp[] = $categories[$i];
            }
        }
        $shop->categories = $temp;
        $shop->save();

        // 同时删除分类下所有商品
        $product = Product::where('belongShopId', $id)->where('belongCategoriesId', $index)->update(array(
            'status' => 'delete'
        ));

        $data = array('code' => 0, 'data' => $categories);
        $output = json_encode($data);
        return $output;
    }

    // 分类排序更新
    public function postUpdateCateOrder(Request $request)
    {
        $result = $this->checkPayload(array("id", "cate"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $cate = $request->input('cate');

        $shop = Shop::find($id);
        $shop->categories = $cate;
        $shop->save();

        $data = array('code' => 0);
        $output = json_encode($data);
        return $output;
    }

    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————





    // ———————————————————————————————————————————————————————————— 订单相关 ——————————————————————————————————————————————————————————

    // 获取订单信息通过id
    public function postFetchOrderInfoById(Request $request)
    {
        $result = $this->checkPayload(array("id"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');

        $order = OrderInfo::where('orderInfo.id', $id)
            ->leftJoin('shop', 'orderInfo.belongShopId', '=', 'shop.id')
            ->select('orderInfo.*', 'shop.name as shopName')
            ->first();
        $data = array('code' => 0, 'data' => $order);
        $output = json_encode($data);
        return $output;
    }


    // 获取订单信息通过用户和状态
    public function postFetchOrderInfoByStatus(Request $request)
    {
        $result = $this->checkPayload(array("belongUserId"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $belongUserId = $request->input('belongUserId');

        $order = OrderInfo::where('orderInfo.belongUserId', $belongUserId)->where('orderInfo.status', '!=', 'cancel')
            ->leftJoin('shop', 'orderInfo.belongShopId', '=', 'shop.id')
            ->select('orderInfo.*', 'shop.name as shopName');
        if (isset($request->status)) {
            $status = $request->input('status');
            $order->where('orderInfo.status', $status);
        }

        $order = $order->orderBy('updateTime', 'DESC')->get();
        $data = array('code' => 0, 'data' => $order);
        $output = json_encode($data);
        return $output;
    }

    // 检查操作在下单前(是否无库存、是否下架)
    public function postCheckStorageBeforeMakeOrder(Request $request)
    {
        $result = $this->checkPayload(array("productList"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $productList = $request->input('productList');
        $temp = array();
        foreach ($productList as $item) {
            $temp[] = $item['id'];
        }

        $output = array();
        $product = Product::whereIn('id', $temp)->get();
        $notAvailable = false;
        foreach ($product as $item) {
            $obj = new \stdClass();
            $obj->id = $item->id;
            $obj->title = $item->title;
            $obj->storageNum = $item->storageNum;
            $obj->status = $item->status;
            $obj->reason = "OK";
            if ($obj->status != 'on') {
                $obj->reason = "产品已下架";
                $notAvailable = true;
            } else {
                foreach ($productList as $inside) {
                    if ($inside['id'] == $item->id) {
                        if ($obj->storageNum - $inside['buyNum'] < 0) {
                            $obj->reason = "产品库存不足";
                            $notAvailable = true;
                        }
                        break;
                    }
                }
            }
            $output[] = $obj;
        }
        if ($notAvailable) {
            $data = array('code' => -1, 'data' => $output);
            $output = json_encode($data);
            return $output;
        }

        $data = array('code' => 0);
        $output = json_encode($data);
        return $output;
    }


    // 更新订单的状态
    public function postUpdateOrderStatus(Request $request)
    {
        $result = $this->checkPayload(array("id", "status"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $status = $request->input('status');

        $order = OrderInfo::find($id);
        $order->status = $status;
        // 如果用户取消订单，则库存、销量退回
        if ($status == 'cancel') {
            foreach ($order->productList as $item) {
                $id = $item['id'];
                $buyNum = $item['buyNum'];
                $product = Product::find($id);
                $product->storageNum += $buyNum;
                $product->soldNum -= $buyNum;
                $product->save();
            }
        }
        $order->save();

        $data = array('code' => 0, 'data' => $order);
        $output = json_encode($data);
        return $output;
    }


    // 用户下单
    public function postInsertOrder(Request $request)
    {
        $result = $this->checkPayload(array("belongUserId", "belongShopId", "paymentType"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $belongUserId = $request->input('belongUserId');
        $belongShopId = $request->input('belongShopId');
        $paymentType = $request->input('paymentType');
        $paymentWay = $request->input('paymentWay');
        $productList = $request->input('productList');
        $total = $request->input('total');
        $deliveryFee = $request->input('deliveryFee');
        $deliveryInfo = $request->input('deliveryInfo');
        $comment = $request->input('comment');

        $orderId = date('YmdHis');
        // 检查重复
        $checkOrder = OrderInfo::where('orderId',$orderId)->count;
        while($checkOrder>0){
            $orderId = date('YmdHis').strval(rand(0,100));
            $checkOrder = OrderInfo::where('orderId',$orderId)->count;
        }
        $paymentId = date('YmdHis');
        $checkPayment = OrderInfo::where('paymentId',$paymentId)->count;
        // 检查重复
        while($checkPayment>0){
            $paymentId = date('YmdHis').strval(rand(0,100));
            $checkPayment = OrderInfo::where('paymentId',$paymentId)->count;
        }
        $status = "pending";
        // if ($paymentType == 'self') {
        //     $status = "wait";
        // }

        $order = OrderInfo::create(array(
            'belongUserId' => $belongUserId,
            'belongShopId' => $belongShopId,
            'paymentType' => $paymentType,
            'paymentWay' => $paymentWay,
            'productList' => $productList,
            'total' => $total,
            'deliveryFee' => $deliveryFee,
            'deliveryInfo' => $deliveryInfo,
            'comment' => $comment,
            'orderId' => $orderId,
            'paymentId' => $paymentId,
            'status' => $status,

        ));

        $order = OrderInfo::find($order->id);


        foreach ($productList as $item) {
            $id = $item['id'];
            $buyNum = $item['buyNum'];
            $product = Product::find($id);
            $product->storageNum -= $buyNum;
            if ($product->storageNum < 0) {
                $data = array('code' => -1, 'data' => $product);
                $output = json_encode($data);
                return $output;
            }
            $product->soldNum += $buyNum;
            $product->save();
        }



        $data = array('code' => 0, 'data' => $order);
        $output = json_encode($data);
        return $output;
    }

    // 上传支付凭证
    public function postUpdateOrderPaymentImg(Request $request)
    {
        $result = $this->checkPayload(array("id", "paymentImg"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $paymentImg = $request->input('paymentImg');
        $paymentWay = $request->input('paymentWay');

        $order = OrderInfo::find($id);
        $order->paymentImg = $paymentImg;
        $order->paymentWay = $paymentWay;
        $order->status = "wait";
        $order->updateTime = date("Y-m-d H:i:s");
        $order->save();

        $belongShopId = $order->belongShopId;
        $shop = Shop::find($belongShopId);

        // 发送短信给商家
        // Your Account SID and Auth Token from twilio.com/console
        $sid = 'AC4a463f4a464fd2f6679942643f4479b1';
        $token = 'c1a12a8a602922f7c94d5111e5417107';
        $client = new Client($sid, $token);
        // Use the client to do fun stuff like send text messages!
        try {
            $client->messages->create(
                // the number you'd like to send the message to
                '+61' . $shop->phone,
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => '+16514018420',
                    // the body of the text message you'd like to send
                    'body' => '您的店铺收到新订单，请登入开店宝小程序查看！'
                )
            );
            $data = array('code' => 0, 'data' => $order);
            $output = json_encode($data);
            return $output;
        } catch (QueryException $e) {
            report($e);
            $data = array('code' => -1, 'errMsg' => 'request faild');
            $output = json_encode($data);
            return $output;
        }


        $data = array('code' => 0, 'data' => $order);
        $output = json_encode($data);
        return $output;
    }

    // 到店自提下单
    public function postUpdateOrderStatusAndPaymentWay(Request $request)
    {
        $result = $this->checkPayload(array("id", "status", "paymentWay"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $status = $request->input('status');
        $paymentWay = $request->input('paymentWay');

        $order = OrderInfo::find($id);
        $order->paymentWay = $paymentWay;
        $order->status = $status;
        $order->save();

        // 发送短信给商家

        $belongShopId = $order->belongShopId;
        $shop = Shop::find($belongShopId);
        // 发送短信给商家
        // Your Account SID and Auth Token from twilio.com/console
        $sid = 'AC4a463f4a464fd2f6679942643f4479b1';
        $token = 'c1a12a8a602922f7c94d5111e5417107';
        $client = new Client($sid, $token);
        // Use the client to do fun stuff like send text messages!
        try {
            $client->messages->create(
                // the number you'd like to send the message to
                '+61' . $shop->phone,
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => '+16514018420',
                    // the body of the text message you'd like to send
                    'body' => '您的店铺收到新订单，请登入开店宝小程序查看！'
                )
            );
            $data = array('code' => 0, 'data' => $order);
            $output = json_encode($data);
            return $output;
        } catch (QueryException $e) {
            report($e);
            $data = array('code' => -1, 'errMsg' => 'request faild');
            $output = json_encode($data);
            return $output;
        }


        $data = array('code' => 0, 'data' => $order);
        $output = json_encode($data);
        return $output;
    }


    // 获取订单通过status和shop id
    public function postFetchOrderInfoByShopId(Request $request)
    {
        $result = $this->checkPayload(array("id"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');

        $order = OrderInfo::where('orderInfo.belongShopId', $id)
            ->leftJoin('shop', 'orderInfo.belongShopId', '=', 'shop.id')
            ->leftJoin('user', 'orderInfo.belongUserId', '=', 'user.id')
            ->select('orderInfo.*', 'shop.name as shopName', 'user.userInfo as userInfo');

        if (isset($request->status)) {
            $status = $request->input('status');
            $order->where('orderInfo.status', $status);
        }

        $order = $order->orderBy('updateTime', 'DESC')->get();

        foreach ($order as $item) {
            $item->userInfo = json_decode($item->userInfo, true);
        }

        $data = array('code' => 0, 'data' => $order);
        $output = json_encode($data);
        return $output;
    }



    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————







    // ———————————————————————————————————————————————————————————— 配送相关 ——————————————————————————————————————————————————————————

    // 计算配送费
    public function postCalHulaDelivery(Request $request)
    {
        $result = $this->checkPayload(array("originArea","destinationArea"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }

        // $origin = $request->input('origin');
        $originArea = $request->input('originArea');
        // $destination = $request->input('destination');
        $destinationArea = $request->input('destinationArea');

        // 如果商家或买家不在墨尔本则无法配送
        if ($originArea != 'VIC' || $destinationArea != 'VIC') {
            $data = array('code' => -1);
            $output = json_encode($data);
            return $output;
        }


        $data = array('code' => 0, 'data'=> 10);
        $output = json_encode($data);
        return $output;
    }

    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————







    // ———————————————————————————————————————————————————————————— 记录信息 ——————————————————————————————————————————————————————————

    // 添加用户访问店铺记录
    public function postInsertVisitInfo(Request $request)
    {
        $result = $this->checkPayload(array("belongUserId", "belongShopId"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $belongUserId = $request->input('belongUserId');
        $belongShopId = $request->input('belongShopId');

        // 检查一小时内最后访问的是否为同一店铺（减少过量数据）
        $date =  date('Y-m-d H:i:s', time() - 60 * 60);
        $visit = VisitInfo::where('belongUserId', $belongUserId)->where('belongShopId', $belongShopId)->where('createTime', '>', $date)->get();

        if (count($visit) > 0) {
            $data = array('code' => 1);
            $output = json_encode($data);
            return $output;
        }

        $bool = VisitInfo::create(array(
            'belongUserId' => $belongUserId,
            'belongShopId' => $belongShopId
        ));

        $data = array('code' => 0);
        $output = json_encode($data);
        return $output;
    }



    // 添加商品访问以及操作情况
    public function postInsertItemInfo(Request $request)
    {
        $result = $this->checkPayload(array("belongUserId", "belongShopId", "belongProductId"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $belongUserId = $request->input('belongUserId');
        $belongShopId = $request->input('belongShopId');
        $belongProductId = $request->input('belongProductId');
        $isDone = $request->input('isDone');
        $tapBuy = $request->input('tapBuy');
        $tapPhone = $request->input('tapPhone');

        $bool = ItemInfo::create(array(
            'belongProductId' => $belongProductId,
            'belongUserId' => $belongUserId,
            'belongShopId' => $belongShopId,
            'isDone' => $isDone,
            'tapBuy' => $tapBuy,
            'tapPhone' => $tapPhone,
        ));

        $data = array('code' => 0, 'data' => $bool);
        $output = json_encode($data);
        return $output;
    }

    // 更新访问情况
    public function postUpdateItemInfo(Request $request)
    {
        $result = $this->checkPayload(array("id"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');

        $item = ItemInfo::find($id);
        if (isset($result->isDone)) {
            $isDone = $request->input('isDone');
            $item->isDone = $isDone;
        }
        if (isset($result->tapBuy)) {
            $tapBuy = $request->input('tapBuy');
            $item->tapBuy = $tapBuy;
        }
        if (isset($result->tapPhone)) {
            $tapPhone = $request->input('tapPhone');
            $item->tapPhone = $tapPhone;
        }

        $item->save();

        $data = array('code' => 0);
        $output = json_encode($data);
        return $output;
    }


    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————



    // ———————————————————————————————————————————————————————————— 权限操作 ——————————————————————————————————————————————————————————

    public function postPermissionCheck(Request $request)
    {
        $result = $this->checkPayload(array("id", "type"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $id = $request->input('id');
        $type = $request->input('type');

        $shop = Shop::find($id);
        $permission = $shop->permission;

        switch ($type) {
            case "product":
                $nowNum = Product::where('belongShopId', $id)->where('status', '!=', 'delete')->count();
                if ($nowNum >= 50) {
                    // 允许新增更多商品
                    if ($permission != NULL && $permission . include('p1')) {
                        $data = array('code' => 0);
                        $output = json_encode($data);
                        return $output;
                    } else {
                        $data = array('code' => -1);
                        $output = json_encode($data);
                        return $output;
                    }
                }
                break;
            case "order":
                $nowNum = OrderInfo::where('createTime', '>', date("y-m-d", strtotime("-30 day")))->where('belongShopId', $id)->where('status', '!=', 'cancel')->count();
                if ($nowNum >= 100) {
                    // 允许新增更多订单
                    if ($permission != NULL && $permission . include('o1')) {
                        $data = array('code' => 0);
                        $output = json_encode($data);
                        return $output;
                    } else {
                        $data = array('code' => -1);
                        $output = json_encode($data);
                        return $output;
                    }
                }

                break;
        }

        $data = array('code' => 0);
        $output = json_encode($data);
        return $output;
    }

    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————



    // ———————————————————————————————————————————————————————————— 其他操作 ——————————————————————————————————————————————————————————


    // 发送定制请求
    public function postSendCustomize(Request $request)
    {
        $result = $this->checkPayload(array(
            "name", "phone", "wxId"
        ), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $name = $request->input('name');
        $phone = $request->input('phone');
        $wxId = $request->input('wxId');

        $bool = Customize::create(array(
            "name" => $name,
            "phone" => $phone,
            "wxId" => $wxId
        ));

        $data = array('code' => 0);
        $output = json_encode($data);
        return $output;
    }

    // 获取banner
    public function postFetchBanner(Request $request)
    {
        $banner = Banner::orderBy('bannerOrder', 'ASC')->get();
        $data = array('code' => 0, 'data' => $banner);
        $output = json_encode($data);
        return $output;
    }


    // 上传图片
    public function postUploadImg(Request $request)
    {
        $imgType = ['image/jpg', 'image/jpeg', 'image/png', 'image/gif', 'video/mp4'];
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $type = $file->getMimeType();
            if (!in_array($type, $imgType)) {
                $data = array('code' => -1, 'errMsg' => 'invalid file');
                $output = json_encode($data);
                return $output;
            }
            $type = explode("/", $type)[1];
            $path = $file->getPathname();

            $str = Str::random(32);
            $s3 = AWS::createClient('s3');
            $s3->putObject(array(
                'Bucket'     => 'weshop',
                'Key'        => 'kaidian/' . $str . '.' . $type,
                'SourceFile' => $path,
                'ACL' => 'public-read'
            ));

            $temp = new \stdClass();
            $temp->url = "https://cdn.weshop.weboostapp.com/kaidian/" . $str . '.' . $type;

            $data = array('code' => 0, 'data' => $temp);
        } else {
            $data = array('code' => -1, 'errMsg' => 'request faild');
        }

        $output = json_encode($data);
        return $output;
    }

    // 发送验证码
    public function postSendVailidation(Request $request)
    {
        $result = $this->checkPayload(array(
            "phone"
        ), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }

        $phone = $request->input('phone');
        $validCode = rand(1000, 9999);

        if (strlen($phone) < 8) {
            $data = array('code' => -1, 'errMsg' => 'invalid phone number');
            $output = json_encode($data);
            return $output;
        }

        // Your Account SID and Auth Token from twilio.com/console
        $sid = 'AC4a463f4a464fd2f6679942643f4479b1';
        $token = 'c1a12a8a602922f7c94d5111e5417107';
        $client = new Client($sid, $token);

        // Use the client to do fun stuff like send text messages!
        try {
            $client->messages->create(
                // the number you'd like to send the message to
                $phone,
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => '+16514018420',
                    // the body of the text message you'd like to send
                    'body' => 'Your validation code: ' . $validCode
                )
            );
            $data = array('code' => 0, 'data' => $validCode);
            $output = json_encode($data);
            return $output;
        } catch (QueryException $e) {
            report($e);
            $data = array('code' => -1, 'errMsg' => 'request faild');
            $output = json_encode($data);
            return $output;
        }
    }


    // 搜索地址
    public function postFetchAddressByGoogle(Request $request)
    {
        $result = $this->checkPayload(array(
            "input"
        ), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }

        $url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyDhkBUsVJZnuLFG-OrRTBzp4qlwAiRUdLU&components=country:au&input=';

        $input = $request->input('input');
        $input = str_replace(" ", "%20", $input);
        $url .= $input;

        $urlData = json_decode(file_get_contents($url));
        if ($urlData != array()) {
            $data = array('code' => 0, 'data' => $urlData);
        } else {
            $data = array('code' => -1);
        }
        $output = json_encode($data);
        return $output;
    }

    // 通过latlng找地址
    public function postFetchAddressByLatlng(Request $request)
    {
        $result = $this->checkPayload(array(
            "latlng"
        ), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=';

        $latlng = $request->input('latlng');
        $latlng = str_replace(" ", "%20", $latlng);
        $url .= $latlng;
        $url .= '&key=AIzaSyDhkBUsVJZnuLFG-OrRTBzp4qlwAiRUdLU';

        if (isset($request->language)) {
            $language = $request->input('language');
            $url .= "&language=";
            $url .= $language;
        }

        if (isset($request->result_type)) {
            $result_type = $request->input('result_type');
            $url .= "&result_type=";
            $url .= $result_type;
        }
        if (isset($request->location_type)) {
            $location_type = $request->input('location_type');
            $url .= "&location_type=";
            $url .= $location_type;
        }

        $urlData = json_decode(file_get_contents($url));

        if ($urlData != array()) {
            $data = array('code' => 0, 'data' => $urlData);
        } else {
            $data = array('code' => -1);
        }
        $output = json_encode($data);
        return $output;
    }

    // 生成小程序二维码
    public function postGenerateMiniProgramQrCode(Request $request)
    {
        $result = $this->checkPayload(array("scene"), $request->all());
        if ($result->getData()->code == -1) {
            return $result;
        }
        $scene = $request->input('scene');

        $param = array(
            'scene' => $scene,
            'page' => 'pages/shop/shop'
        );

        $endpoint = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxb75881f7c8929931&secret=9e1be66e0c1afc7905e16a4c76b13dbf";
        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', $endpoint);
        $content = $response->getBody();
        $content = json_decode($content);
        $accessKey = $content->access_token;
        $callUrl = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" . $accessKey;


        $client = new \GuzzleHttp\Client();

        $response = $client->post(
            $callUrl,
            array(
                'headers' => array(
                    'content-type' => 'image/jpeg'
                ),
                'json' => $param
            )
        );

        $data = $response->getBody()->getContents();

        $str = Str::random(32);
        $s3 = AWS::createClient('s3');
        $s3->putObject(array(
            'Bucket'     => 'weshop',
            'Key'        => 'kaidian/' . $str . '.png',
            'Body'       => $data,
            'ContentEncoding' =>  'base64',
            'ContentType' => 'image/png',
            'ACL' => 'public-read'

        ));

        $temp = new \stdClass();
        $temp->url = "https://cdn.weshop.weboostapp.com/kaidian/" . $str . '.png';



        $data = array('code' => 0, 'data' => $temp);
        $output = json_encode($data);
        return $output;
    }

    // 控制审核
    public function postFetchControl(Request $request)
    {
        $data = array('code' => 0);
        $output = json_encode($data);
        return $output;
    }

    // 控制审核第二类（用于区别正式版）
    public function postFetchControlSec(Request $request)
    {
        $data = array('code' => -1);
        $output = json_encode($data);
        return $output;
    }

    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————



}
