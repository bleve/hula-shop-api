<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Database\QueryException;
use Exception;

use App\Model\Shop;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    // ———————————————————————————————————————— 数据库功能 ——————————————————————————————————————————————————————————————

    // 获取该model所有的数据
    public function allData($model)
    {
        $model = 'App\Model\\' . $model;
        $data = $model::all();
        return $data;
    }

    // 获取该model的数据通过page
    public function pageData($model, $num, $textfilter = NULL, $textSimilarfilter = NULL, $jsonArrFilter = NULL, $jsonFilter = [], $orderFilter = NULL, $customize = false)
    {
        $model = 'App\Model\\' . $model;
        $data = $model::query();

        // 通过文字进行筛选
        foreach ($textfilter as $key => $value) {
            if (!is_array($value)) {
                $value = [$value];
            }
            $data = $data->whereIn($key, $value);
        }

        // 通过文字进行相似查询
        foreach ($textSimilarfilter as $key => $value) {
            $data = $data->where($key, 'LIKE', '%' . $value . '%');
        }

        // // 通过JSON array进行搜索
        // foreach ($jsonArrFilter as $key => $value) {
        //     $data = $data->whereRaw("JSON_CONTAINS(" . $key . ", '[$value]' )");
        // }

        // // 通过JSON进行搜索 --暂时不使用 ["XX","XX","XXX]
        // foreach ($jsonFilter as $item) {
        //     $data = $data->whereRaw("JSON_VALUE(`" . $item[0] . "`, '$." . $item[1] . "') LIKE '%" . $item[2] . "%'");
        // }

        // 进行排序 {"id":"DESC","createTime":"DESC"}
        foreach ($orderFilter as $key => $value) {
            $data = $data->orderBy($key, $value);
        }

        // 是否自定义
        try {
            if (!$customize) {
                $data = $data->paginate($num);
            }
        } catch (QueryException $e) {
            return $this->returnCode(500);
        }
        return $this->returnCode(200, $data);
    }

    // 增加特定Model数据
    public function insertData($model, $obj)
    {
        $arr = (array) $obj;
        $model = 'App\Model\\' . $model;
        $checkModel = new $model;
        $checkFillable = $checkModel->getFillable();
        $checkFillable = array_flip($checkFillable);
        // print_r($checkFillable);

        // // 检查创建前必填信息是否都有
        // if (!$this->compareIfObjectHaveSameKey($checkFillable, $obj)) {
        //     return $this->returnCode(500);
        // }
        try {
            $data = $model::create($arr);
            return $this->returnCode(200, $data->id);
        } catch (QueryException $e) {
            return $this->returnCode(500);
        }
    }

    // 更新特定Model数据
    public function updateData($model, $id, $obj)
    {
        $model = 'App\Model\\' . $model;
        $data = $model::find($id);
        foreach ($obj as $key => $value) {
            $data->$key = $value;
        }
        try {
            $data->save();
            return $this->returnCode(200, $data->id);
        } catch (QueryException $e) {
            return $this->returnCode(500);
        }
    }

    // 删除特定Model数据
    public function deleteData($model, $id, $obj)
    {
        $model = 'App\Model\\' . $model;
        $data = $model::find($id);
        try {
            $data->delete();
            return true;
        } catch (QueryException $e) {
            return false;
        }
    }

    public function orderByData($type)
    {
        switch ($type) {
            case "newAsc":
                return ["createTime", "ASC"];
                break;
            case "newDesc":
                return ["createTime", "DESC"];
                break;
            case "priceAsc":
                return ["price", "ASC"];
                break;
            case "priceDesc":
                return ["price", "DESC"];
                break;
        }
    }


    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————






    // ———————————————————————————————————————— 常用功能 ——————————————————————————————————————————————————————————————

    // 清除特定符号
    public function regex($strParam)
    {
        $regex = "/\/|\～|\，|\。|\！|\？|\“|\”|\【|\】|\『|\』|\：|\；|\《|\》|\’|\‘|\ |\·|\~|\!|\@|\#|\\$|\%|\^|\&|\*|\(|\)|\_|\+|\{|\}|\:|\<|\>|\?|\[|\]|\,|\.|\/|\;|\'|\`|\-|\=|\\\|\|/";
        return preg_replace($regex, "", $strParam);
    }

    // 比较两个object内是否有完全相同的key
    public function compareIfObjectHaveSameKey($a, $b)
    {
        $aKeys = array_keys(json_decode(json_encode($a), true));
        $bKeys = array_keys(json_decode(json_encode($b), true));
        if (count(array_diff($aKeys, $bKeys)) != 0) {
            return false;
        }
        return true;
    }

    // 清除部分object attribute
    public function unsetAttribute($obj,$deleteArr)
    {
        foreach($deleteArr as $item){
            unset($obj->$item);
        }
        return $obj;
    }


    // 检查payload是否包含需求的数据
    public function checkPayload($requiredPayload, $payload = NULL)
    {
        if ($payload == NULL) {
            return $this->returnCode(500);
        }
        $payloadKeys = array_keys(json_decode(json_encode($payload), true));
        if (count(array_diff($requiredPayload, $payloadKeys)) != 0) {
            return $this->returnCode(500);
        }
        return $this->returnCode(200);
    }

    // 返回数据结构
    public function returnCode($code, $value = NULL, $errMsg = "Request Failed")
    {
        switch ($code) {
            case "200":
                $data = array('code' => 200, 'data' => $value);
                break;
            default:
                $data = array('code' => 500, 'errMsg' => $errMsg);
                break;
        }
        return response()->json($data);
    }


    public function checkShopRole($id)
    {
        $shop = Shop::find($id);
        if ($shop->hasRole(['client 1'])) {
            return 1;
        }
        if ($shop->hasRole(['client 2'])) {
            return 2;
        }
        return 0;
    }

    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————



}
