<?php

namespace App\Http\Controllers;

use App\Model\Categories;
use Illuminate\Http\Request;
use App\Model\OrderInfo;
use App\Model\Shop;
use App\Model\Product;
use App\Model\ProductNorm;
use AWS;
use Illuminate\Support\Str;

class adminController extends Controller
{


    // ———————————————————————————————————————— 账号功能 ——————————————————————————————————————————————————————————————


    // 登入商家后台
    public function adminLogin(Request $request)
    {
        $result = $this->checkPayload(array("username", "password"), $request->all());
        if ($result->getData()->code == 500) {
            return $result;
        }

        $username = $request->input('username');
        $password = $request->input('password');
        $shop = Shop::where('username', $username)->first();
        if ($shop == null) {
            return $this->returnCode(500, null, 'username not found');
        }

        $password = md5($password . $shop->salt);
        $shop = Shop::where('username', $username)->where('password', $password)->first();
        if ($shop == null) {
            return $this->returnCode(500, null, 'username not found');
        }

        $result = $this->checkShopRole($shop->id);
        $shop->auth = Str::random(40);
        $shop->save();
        $data = $shop->auth;

        switch ($result) {
            case 0:
                return $this->returnCode(500);
                break;
            default:
                return $this->returnCode(200, $data);
                break;
        }
    }

    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————





    // ———————————————————————————————————————— 订单功能 ——————————————————————————————————————————————————————————————

    // 获取订单
    public function adminFetchOrderByPage(Request $request)
    {
        $belongShopId = $request->get('belongShopId');
        $data = OrderInfo::where('belongShopId', $belongShopId);


        if (isset($request->status)) {
            $status = $request->input('status');
            $data->whereIn('status', $status);
        }

        if (isset($request->keyword)) {
            $keyword = $request->input('keyword');
            $data->where('orderId', 'Like', '%' . $keyword . '%')->orWhere('deliveryInfo', 'Like', '%' . $keyword . '%');
        }

        if (isset($request->orderBy)) {
            $orderBy = $request->input('orderBy');
            $result = $this->orderByData($orderBy);
            $data->orderBy($result[0], $result[1]);
        }

        $data = $data->paginate(50);
        return $this->returnCode(200, $data);
    }



    // 更新订单信息
    public function adminUpdateOrder(Request $request)
    {
        $result = $this->checkPayload(array("orderId"), $request->all());
        if ($result->getData()->code == 500) {
            return $result;
        }

        $orderId = $request->input('orderId');
        $belongShopId = $request->get('belongShopId');

        $order = OrderInfo::where('orderId', $orderId)->where('belongShopId', $belongShopId)->first();
        if ($order == null) {
            return $this->returnCode(500, null, 'Order Id not found');
        }
        $obj = (object)$request->all(); 
        $obj = $this->unsetAttribute($obj, array("orderId", "belongShopId", "belongUserId","createTime","auth"));

        return $this->updateData('OrderInfo', $order->id, $obj);
    }


    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————



    // ———————————————————————————————————————— 商品功能 ——————————————————————————————————————————————————————————————


    // 获取商品list
    public function adminFetchProductByPage(Request $request)
    {
        $belongShopId = $request->get('belongShopId');
        $data = Product::where('product.belongShopId', $belongShopId);

        if (isset($request->title)) {
            $title = $request->input('title');
            $data->where('title', 'Like', '%' . $title . '%');
        }

        if (isset($request->status)) {
            $status = $request->input('status');
            $data->whereIn('product.status', $status);
        }

        if (isset($request->orderBy)) {
            $orderBy = $request->input('orderBy');
            $result = $this->orderByData($orderBy);
            $data->orderBy($result[0], $result[1]);
        }

        $data = $data
            ->leftJoin('categories', 'product.belongCategoriesId', '=', 'categories.id')
            ->orderBy('product.id', 'DESC')
            ->select('product.*', 'categories.name as categoriesName')
            ->paginate(50);
        return $this->returnCode(200, $data);
    }


    // 创建新商品
    public function adminInsertProduct(Request $request)
    {
        $belongShopId = $request->get('belongShopId');
        $result = $this->checkShopRole($belongShopId);

        $obj = (object)$request->all();
        $obj = $this->unsetAttribute($obj, array("belongShopId", "auth", "skuId"));

        $shop = Shop::find($belongShopId);
        $currentNum = Product::where('belongShopId', $belongShopId)->count();

        $obj->belongShopId = $belongShopId;
        $obj->skuId = $shop->skuKey . ($currentNum + 1);

        switch ($result) {
            case 0:
                return $this->returnCode(500);
                break;
            case 2:
                $obj->display = "on";
                break;
            default:
                $obj->display = "off";
                break;
        }

        return $this->insertData('Product', $obj);
    }

    // 编辑商品
    public function adminUpdateProduct(Request $request)
    {
        $result = $this->checkPayload(array("productId"), $request->all());
        if ($result->getData()->code == 500) {
            return $result;
        }
        $productId = $request->input('productId');
        $obj = (object)$request->all();
        $obj = $this->unsetAttribute($obj, array("belongShopId", "auth", "skuId", "productId"));
        return $this->updateData('Product', $productId, $obj);
    }

    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————


    // ———————————————————————————————————————— 类别功能 ——————————————————————————————————————————————————————————————

    // 获取分类list
    public function adminFetchCategoriesByPage(Request $request)
    {
        $belongShopId = $request->get('belongShopId');
        $data = Categories::where('belongShopId', $belongShopId);

        if (isset($request->name)) {
            $name = $request->input('name');
            $data->where('name', 'Like', '%' . $name . '%');
        }

        if (isset($request->status)) {
            $status = $request->input('status');
            $data->whereIn('status', $status);
        }

        $data = $data->paginate(50);
        return $this->returnCode(200, $data);
    }

    // 获取商家下的所有分类
    public function adminFetchCategoriesByShop(Request $request)
    {
        $belongShopId = $request->get('belongShopId');
        $data = Categories::where('belongShopId', $belongShopId)->where('status', 'on')->get();
        return $this->returnCode(200, $data);
    }

    // 创建新分类
    public function adminInsertCategories(Request $request)
    {
        $belongShopId = $request->get('belongShopId');
        $obj = (object)$request->all();
        $obj->belongShopId = $belongShopId;
        return $this->insertData('Categories', $obj);
    }

    // 编辑分类
    public function adminUpdateCategories(Request $request)
    {
        $result = $this->checkPayload(array("categoriesId"), $request->all());
        if ($result->getData()->code == 500) {
            return $result;
        }

        $categoriesId = $request->input('categoriesId');
        $obj = (object)$request->all();
        $obj = $this->unsetAttribute($obj, array("belongShopId", "auth", "categoriesId"));

        // 如果更新状态则更新所有商品
        if (isset($request->status)) {
            Product::where('belongCategoriesId', $categoriesId)->update(array('status' => $obj->status));
        }

        return $this->updateData('Categories', $categoriesId, $obj);
    }


    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————


    // ———————————————————————————————————————— 规格功能 ——————————————————————————————————————————————————————————————



    // 创建新规格
    public function adminInsertNorm(Request $request)
    {

        $result = $this->checkPayload(array("productId"), $request->all());
        if ($result->getData()->code == 500) {
            return $result;
        }

        $productId = $request->input('productId');
        $belongShopId = $request->get('belongShopId');

        $obj = (object)$request->all();
        $obj = $this->unsetAttribute($obj, array("belongShopId", "belongProductId", "productId"));

        $obj->belongShopId = $belongShopId;
        $obj->belongProductId = $productId;

        return $this->insertData('ProductNorm', $obj);
    }

    // 获取商品下的规格
    public function adminFetchNormByProduct(Request $request)
    {
        $result = $this->checkPayload(array("productId"), $request->all());
        if ($result->getData()->code == 500) {
            return $result;
        }

        $productId = $request->get('productId');
        $product = Product::find($productId);

        $data = ProductNorm::where('belongProductId', $productId)->whereIn('status', ['on', 'off'])->get();

        return $this->returnCode(200, $data);
    }

    // 编辑规格
    public function adminUpdateNorm(Request $request)
    {
        $result = $this->checkPayload(array("normId"), $request->all());
        if ($result->getData()->code == 500) {
            return $result;
        }

        $normId = $request->input('normId');
        $obj = (object)$request->all();
        $obj = $this->unsetAttribute($obj, array("belongShopId", "belongProductId", "auth", "soldNum", "normId"));

        return $this->updateData('ProductNorm', $normId, $obj);
    }

    // 获取规格以及商品信息
    public function adminFetcNormAndProductByIds(Request $request)
    {
        $result = $this->checkPayload(array("ids"), $request->all());
        if ($result->getData()->code == 500) {
            return $result;
        }

        $ids = $request->get('ids');
        $norm = ProductNorm::whereIn('productNorm.id', $ids)
            ->leftJoin('product', 'product.id', '=', 'productNorm.belongProductId')
            ->select('productNorm.*', 'product.skuId', 'product.title')
            ->get();

        return $this->returnCode(200, $norm);
    }




    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————





    // ———————————————————————————————————————— 基础功能 ——————————————————————————————————————————————————————————————

    public function adminUpload(Request $request)
    {
        $imgType = ['image/jpg', 'image/jpeg', 'image/png', 'image/gif', 'video/mp4'];
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $type = $file->getMimeType();
            if (!in_array($type, $imgType)) {
                $data = array('code' => -1, 'errMsg' => 'invalid file');
                $output = json_encode($data);
                return $output;
            }
            $type = explode("/", $type)[1];
            $path = $file->getPathname();

            $str = Str::random(32) . strval(time());
            $s3 = AWS::createClient('s3');
            $s3->putObject(array(
                'Bucket'     => 'bforce-hula',
                'Key'        => $str . '.' . $type,
                'SourceFile' => $path,
            ));

            $data = "https://hulacdn.weboostapp.com/" . $str . '.' . $type;

            return $this->returnCode(200, $data);
        } else {
            return $this->returnCode(500);
        }
    }

    // ——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

}
