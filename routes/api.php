<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {

    Route::post('postFetchProductByPage', 'clientController@postFetchProductByPage');
    Route::post('postFetchOrderByPage', 'clientController@postFetchOrderByPage');
    Route::post('postInsertOrder', 'clientController@postInsertOrder');
    Route::post('postCancelOrder', 'clientController@postCancelOrder');

    Route::post('adminLogin', 'adminController@adminLogin');
    Route::post('adminUpload', 'adminController@adminUpload');
    Route::post('test', 'testController@test');
    
});

Route::group(['middleware' => ['admin'], 'prefix' => 'v1'], function () {

    Route::post('adminFetchProductByPage', 'adminController@adminFetchProductByPage');
    Route::post('adminInsertProduct', 'adminController@adminInsertProduct');
    Route::post('adminUpdateProduct', 'adminController@adminUpdateProduct');


    Route::post('adminFetchNormByProduct', 'adminController@adminFetchNormByProduct');
    Route::post('adminInsertNorm', 'adminController@adminInsertNorm');
    Route::post('adminUpdateNorm', 'adminController@adminUpdateNorm');
    Route::post('adminFetcNormAndProductByIds', 'adminController@adminFetcNormAndProductByIds');
    
    Route::post('adminFetchOrderByPage', 'adminController@adminFetchOrderByPage');
    Route::post('adminUpdateOrder', 'adminController@adminUpdateOrder');

    Route::post('adminFetchCategoriesByPage', 'adminController@adminFetchCategoriesByPage');
    Route::post('adminInsertCategories', 'adminController@adminInsertCategories');
    Route::post('adminUpdateCategories', 'adminController@adminUpdateCategories');
    Route::post('adminFetchCategoriesByShop', 'adminController@adminFetchCategoriesByShop');

    
});
